Description
-----------
This module should check the title of a node before it is saved and remove any non-alphanumeric characters but leave spaces..  

The admin config page will allow selection of which content types should have their titles cleaned of non-alphanumeric characters (leave spaces in). Only those node types that are selected on the admin config page should have their title’s cleaned. 

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire webform directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Configure the content type at admin/config/title-clean


